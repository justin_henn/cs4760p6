//This is the slave file for the oss/slave program
//Justin Henn
//Assignment 6
//5/03/17
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/msg.h>
#include <unistd.h>
#include "msgqueue.h"
#define PERM (S_IRUSR | S_IWUSR)
#define NUM_RESOURCES 20
#define BOUND 50001
#define MULTIPLY_FOR_SECOND 1000000000
#define MULTIPLY_FOR_MILLI 1000000
#define TOTAL_P_TABLE 576
#define PAGE_PER_PROCESS 32


//structure for message

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;

//timer for when to release or request resource

/*int when_to_resource(int* s, int* n) {

  return (rand() % BOUND) + ((*s * MULTIPLY_FOR_SECOND) + *n);

}

//choose what resource to request

int choose_resource_to_request(resource_t* res, int ele) {

  while(1) {

    int i = rand() % NUM_RESOURCES;
    if ((&res[i])->request[ele] < (&res[i])->all_available)
      return i;
  }
}

//choose what resource to release

int choose_resource_to_release(resource_t* res, int ele) {

  int choose_arr[NUM_RESOURCES];
  int i = rand() % NUM_RESOURCES;


  while ((&res[i])->allocation[ele] == 0 || (&res[i])->release[ele] == (&res[i])->allocation[ele] ) {

      i = rand() % NUM_RESOURCES;
  }
  (&res[i])->release[ele]++;
  return i;
}


//choose to release or request

int what_to_do(resource_t* res, int ele) {

  int i;
  for(i = 0; i < NUM_RESOURCES; i++) {

    if ((&res[i])->allocation[ele] > 0 && (&res[i])->release[ele] < (&res[i])->allocation[ele] ) {

      return rand() % 2;
    }
  }
  return 0;
}*/

//timer for when to check to terminate

int get_term_time() {

   return rand() % 201 + 900;

}

//odds of terminating when checking timer

int should_proc_term() {

  return rand() % 3;

}

//pick a page to use

int pick_address() {

  return rand() % 33000;

}

//pick read or write

int what_memory_operation() {

  return rand() % 5 + 1;
}

int main (int argc, char **argv) {

  key_t key_seconds, key_p_table, key_nano_seconds, child_queue_key, parent_queue_key, kill_queue_key, key_resources, key_pids, time_queue_key, deadlock_queue_key, deadlockrec_queue_key, key_locks;
  key_p_table = 3423563;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  key_seconds = 84323;
  kill_queue_key = 000001;
  key_resources = 010001;
  key_pids = 100011;
  time_queue_key = 100012;
  deadlock_queue_key = 100022;
  deadlockrec_queue_key = 100033;
  int* seconds;
  int* nano_seconds;
  int* pids;
  int* p_table;
  int* locks;
  key_locks = 100013;
  //int proc_num = atoi(argv[1]);
  //int num_incr = atoi(argv[3]);
  //int number_of_proc = atoi(argv[4]);
  int shm_id_p_table, i, shm_id_nano_seconds, j, shm_id_seconds, request_or_release = 5, time_to_change_resources, shm_id_resources, shm_id_pids, shm_id_locks;
  //FILE* logfile;
  //struct timespec tps;
  static int child_queueid, parent_queueid, kill_queueid, time_queueid, deadlock_queueid, deadlockrec_queueid;
  int size;
  mymsg_t mymsg, sentmsg, parentmsg, timemsg;
  srand(time(NULL));
  int full_nano = 0, timer = 0, x = 0, what_time_to_resource = 0, what_resource_to_release, what_resource_to_request, time_to_check_for_term, read_or_write;
  int what_proc_am_i, what_message_to_check, proc_num = atoi(argv[1]);
  //pcb_info_t* pcb_for_project;
  int num_references = 0, what_address;
  //srand(time(NULL));

//open shared memory

  if ((shm_id_locks = shmget(key_locks, proc_num*sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((locks = (int*)shmat(shm_id_locks, NULL, 0)) == (void*)-1) {
    perror("pids Failed to attach shared memory segment pids");
    return 1;
  }

  if ((shm_id_pids = shmget(key_pids, proc_num*sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((pids = (int*)shmat(shm_id_pids, NULL, 0)) == (void*)-1) {
    perror("pids Failed to attach shared memory segment pids");
    return 1;
  }

  if ((shm_id_p_table = shmget(key_p_table, TOTAL_P_TABLE*sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((p_table = (int*)shmat(shm_id_p_table, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment ptable");
    return 1;
  }


  if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment seconds");
    return 1;
  }


  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((nano_seconds = (int *)shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
      perror("Failed to attach shared memory segment nanseconds");
      return 1;
  }
  child_queueid = initqueue(child_queue_key, child_queueid);
  parent_queueid = initqueue(parent_queue_key, parent_queueid);
  kill_queueid = initqueue(kill_queue_key, kill_queueid);
  time_queueid = initqueue(time_queue_key, time_queueid);
  deadlock_queueid = initqueue(deadlock_queue_key, deadlock_queueid);
  //deadlockrec_queueid = initqueue(deadlockrec_queue_key, deadlockrec_queueid);

//get your element in the pids array

  while (pids[x] != getpid())
   x++;

  int what_is_my_element = x;
  timemsg.mtype = 1;


  time_to_check_for_term = get_term_time();

  if(msgsnd(time_queueid, &timemsg, 1024,0) == -1) {
    perror("Failed write");
    return 1;
  }
  sentmsg.mtype = getpid();
  mymsg.mtype = getpid();

//main loop

  while (1) {

//pick what page and whether read or write

    what_address = pick_address();
    read_or_write = what_memory_operation();

//if it is write


    if (read_or_write == 5) {

      sprintf(sentmsg.mtext, "%d", what_address);

      if(msgsnd(child_queueid /*write queue*/, &sentmsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
      }
      locks[what_is_my_element] = 0;
      while(locks[what_is_my_element] == 0)
        ;

/*      if ((msgrcv(time_queueid, &timemsg, 1024, getpid(), 0)) >=0) {
        ;
      }*/
    }


//if it is read

    else {

      sprintf(sentmsg.mtext, "%d", what_address);

      if(msgsnd(parent_queueid /*read queue*/, &sentmsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
      }
      locks[what_is_my_element] = 0;
      while(locks[what_is_my_element] == 0)
        ;

/*      if ((msgrcv(time_queueid, &timemsg, 1024, getpid(), 0)) >=0) {
        ;
      }*/
    }
    num_references++;

//check to see if it should terminate successfully

    if (num_references >= time_to_check_for_term) {

      if (should_proc_term() == 2) {

        if(msgsnd(kill_queueid/*term queue*/, &mymsg, 1024,0) == -1) {
          perror("Failed write");
          return 1;
        }
        if ((msgrcv(deadlock_queueid, &timemsg, 1024, getpid(), 0)) >=0) {
        ;
        }
        break;
      }
      else {

        time_to_check_for_term = get_term_time();
        num_references = 0;
      }
    }
  }
  return 0;
}
