//main
//This is the main file and the master file for the oss/slave program
//Justin Henn
//5/03/17
//Assignment 6
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#include <memory.h>
#include "msgqueue.h"
#include "queue.h"
#define PERM (S_IRUSR | S_IWUSR)
#define MULTIPLY_FOR_MILLI 1000000
#define MULTIPLY_FOR_SECOND 1000000000
#define SPAWN_PROC (rand() % 500 + 1) + ((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds);
#define TOTAL_LINES 100000
#define TOTAL_P_TABLE 576
#define TOTAL_FRAMES 256
#define PAGE_PER_PROCESS 32
#define MAX_PROC 18

/*typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;*/


int num_proc_for_alarm, shm_id_p_table, shm_id_nano_seconds, shm_id_seconds, shm_id_resources, shm_id_pids, shm_id_locks;
int deadlocked_proc_terminated = 0, granted = 0, success_term = 0, detection_ran = 0, total_page_faults = 0, total_memory_access = 0;
static int child_queueid, parent_queueid, kill_queueid, time_queueid, deadlock_queueid, deadlockrec_queueid;
Queue* q1;
int* seconds;
int* nano_seconds;
int* pids;
int* p_table;
int* locks;
//resource_t* all_resources;
FILE* logfile;


//kill child processes

void kill_childs () {

  int y, z = num_proc_for_alarm;
  signal(SIGTERM, SIG_IGN);
  kill(0, SIGTERM);
  for(y = 0; y < num_proc_for_alarm; y++){

    if (wait(NULL) != -1) {

      z--;
    }
  }
}


//detach and remove shared memory

int detachandremove (int shmid, void *shmaddr) {
   int error = 0;

   if (shmdt(shmaddr) == -1)
     error = errno;
   if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !error)
     error = errno;
   if (!error)
      return 0;
   return -1;
}

//print statistics at end of run

void statistics() {

  fprintf(logfile, "\n\nNumber of accesses: %d, number of seconds: %d, Accesses Per Second: %.6f\n",total_memory_access, *seconds, (double)total_memory_access / *seconds);
  fprintf(logfile, "Number of Page Faults: %d, number of accesses: %d, Faults per access: %.6f\n", total_page_faults, total_memory_access, (double)total_page_faults / total_memory_access);
  fprintf(logfile, "Average memory speed %.6f nano seconds\n", 10 + ((double)total_page_faults / total_memory_access) * 5);
//  fprintf(logfile, "How many times deadlock detection was run: %d\n", detection_ran);

}

//Alarm handler

void ALARM_handler(int sig) {

/*  int y;
  fprintf(stderr, "Reached alarm\n");
  for(y = 0; y < num_proc_for_alarm; y++)
    kill(point[y], SIGKILL); */


  printf("Alarm Reached\n");
  kill_childs();

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_p_table, p_table) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if (detachandremove(shm_id_pids, pids) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
  if(removequeue(time_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlock_queueid) < 0)
    perror("remove queue");
  /*if(removequeue(deadlockrec_queueid) < 0)
    perror("remove queue");*/

  statistics();

  queueDestroy(q1);

  fclose(logfile);

  exit(0);
}

//Control C handler

void SIGINT_handler(int sig) {

  signal(sig, SIG_IGN);
  printf("Received Control C\n");
  kill_childs();

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_p_table, p_table) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if (detachandremove(shm_id_pids, pids) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_locks, locks) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
  if(removequeue(time_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlock_queueid) < 0)
    perror("remove queue");

  //statistics();

  queueDestroy(q1);
  fclose(logfile);

  exit(0);
}


//get the pid of the calling message

int get_pid_number (int a[], int proc_num, mymsg_t m) {

  int x;
  for (x = 0; x < proc_num; x++) {

    if(a[x] == m.mtype)
      return x;
  }
  return -1;
}

//set frame table as free


void set_frame_table(char frames[]) {

  int i;
  for(i = 0; i < TOTAL_FRAMES; i++) {

    frames[i] = 'F';
  }
}

//check to see if the page is already in a frame and if that frame is still valid

int is_page_in_frame(int x, mymsg_t m, char frames[]) {

  int i  = (x * PAGE_PER_PROCESS) + (atoi(m.mtext) / 1000);
  if (p_table[i] != -1 && frames[p_table[i]] == 'V') {

    return 1;
    }
  else if (p_table[i] != -1 && frames[p_table[i]] == 'U') {

    frames[p_table[i]] = 'V';
    return 1;
  }
  else
    return 0;
}

//checks to see if memory is full

int memory_full(char f[]) {

  int i;
  for (i = 0; i < TOTAL_FRAMES; i++) {

    if (f[i] == 'F')
      return 1;

    if (f[i] == 'U')
      return 1;
  }
  return 0;
}

//finds a free frame to use for a new page

int find_a_frame(char f[]) {

//printf("Hello\n");
  int i, z;
  for (i = 0; i < TOTAL_FRAMES; i++) {

    if (f[i] == 'F') {

      f[i] = 'V';
      return i;
    }

    if (f[i] == 'U') {

      for (z = 0; z < TOTAL_P_TABLE; z++) {

        if (p_table[z] == f[i]) {

          p_table[z] = -1;
          f[i] = 'V';
          return i;
        }
      }
    }
  }
}


//fills the page table with -1 to show it's a page that is not in a frame

void fill_p_table() {

 int i;
 for(i = 0; i < TOTAL_P_TABLE; i++)
  p_table[i] = -1;
}

void reclaim_frames(char f[], int x) {

  int i = x * PAGE_PER_PROCESS;
  int z = i + PAGE_PER_PROCESS;

  for (i; i < z; i++) {

    if(p_table[i] != -1) {

      f[p_table[i]] = 'F';
      p_table[i] = -1;
    }
  }
}

//see if the queue for process pages is full

int full_queue(int in_queue[], int tot) {

  int i;
  for(i = 0; i < tot; i++) {

    if(in_queue[i] == 0)
      return 0;
  }
    return 1;
}


//does the frame validity algorithm to update the frame table

void get_page(char f[]) {

  int i = 0, x;

  for (x = 0; x < TOTAL_FRAMES; x++) {

    if (f[x] =='U')
      f[x] = 'F';

    if(f[x] == 'V') {

      f[x] = 'U';
      i++;
    }
    if (i >= 13)
      break;
  }
}

int main (int argc, char **argv) {

  int opt;
  char* filename = NULL;
  int number_proc = 0, verbose = 0, i, x, y, processes_made = 0, z, total_num_of_proc = 8;
  long  unsigned when_to_spawn = 0;
  extern char* optarg;
  //int pids[total_num_of_proc];
  key_t key_p_table, key_nano_seconds, key_resources, child_queue_key, parent_queue_key, key_seconds, kill_queue_key, key_pids, time_queue_key, deadlock_queue_key, deadlockrec_queue_key, key_locks;
  key_p_table = 3423563;
  key_seconds = 84323;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  kill_queue_key = 000001;
  key_resources = 010001;
  key_pids = 100011;
  time_queue_key = 100012;
  deadlock_queue_key = 100022;
  deadlockrec_queue_key = 100033;
  key_locks = 100013;
  //FILE* logfile;
  pid_t child_pid;
  unsigned char bit_vector[3];
  int iter = 0, iter1 = 0, checked_message, overhead = 0, num_lines = 0, full_time = 0, free_frames = 0, alarm_num = 20;
  char send_str[1024];
  char frames[TOTAL_FRAMES];
  srand(time(NULL));
  int in_queue[total_num_of_proc], time_to_print = 0, page_faults[total_num_of_proc], no_faults[total_num_of_proc];
  int dirty_bit[TOTAL_P_TABLE], r = 0;
  q1 = make_queue();
  signal(SIGINT, SIGINT_handler);
  signal(SIGALRM, ALARM_handler);

  mymsg_t mymsg, sentmsg, timemsg, tempmsg;

//Get arguments

  while ((opt = getopt(argc, argv, "hs:l:v:t:")) != -1) { //this loop looks at the the arguments and processes them appropriately

    switch (opt) {
    case 'h':
      printf("h - help\nn - specify a value after n for the error code \nl - specify a file name after l for the logfile \ns - number of slave processes\nv - 1 for verbose\nt - alarm timer\n");
      return 0;
    case 's':
      total_num_of_proc = atoi(optarg);
      break;
    case 'l':
      filename = optarg;
      break;
    case 'v':
      verbose  = atoi(optarg);
      break;
    case 't':
      alarm_num = atoi(optarg);
      break;
    }
  }

//fill argument

  if (filename == NULL) { //if no filename was givien as an argument

    filename = "test.out";
  }

//make sure maximun processes less than or equal to 18

if (total_num_of_proc > MAX_PROC)
   total_num_of_proc = MAX_PROC;

//open file

  if ((logfile = fopen(filename, "w+")) == NULL) { //check to see if it opened a file

    perror("File open error");
    return -1;
  }

//open shared memory

  if ((shm_id_locks = shmget(key_locks, total_num_of_proc*sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL pids");
    return 1;
  }
  if ((locks = (int*)shmat(shm_id_locks, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment pids");
    if (shmctl(shm_id_locks, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  if ((shm_id_pids = shmget(key_pids, total_num_of_proc*sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL pids");
    return 1;
  }
  if ((pids = (int*)shmat(shm_id_pids, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment pids");
    if (shmctl(shm_id_pids, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  if ((shm_id_p_table = shmget(key_p_table, TOTAL_P_TABLE*sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((p_table = (int*)shmat(shm_id_p_table, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment ptable");
    if (shmctl(shm_id_resources, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

   if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment seconds");
    if (shmctl(shm_id_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  //get shared memory for nano_seconds;
  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((nano_seconds = (int*)shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment nano seconds");
    if (shmctl(shm_id_nano_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  //setting memory

  memset(pids, 0, sizeof(pids));
  memset(in_queue, 0, sizeof(in_queue));
  memset(page_faults, 0, sizeof(page_faults));
  memset(no_faults, 0, sizeof(no_faults));
  memset(dirty_bit, 0, sizeof(dirty_bit));
  fill_p_table();

//open queues

  child_queueid = initqueue(child_queue_key, child_queueid);
  parent_queueid = initqueue(parent_queue_key, parent_queueid);
  kill_queueid = initqueue(kill_queue_key, kill_queueid);
  time_queueid = initqueue(time_queue_key, time_queueid);
  deadlock_queueid = initqueue(deadlock_queue_key, deadlock_queueid);
  //deadlockrec_queueid = initqueue(deadlockrec_queue_key, deadlockrec_queueid);

set_frame_table(frames);
int test_number = 0;

//set up timers for actions of oss

  when_to_spawn = SPAWN_PROC
  sprintf(send_str, "%d", total_num_of_proc);

    alarm(alarm_num);

//main loop

  while (1) {

//spawn new process at timer

    if (((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds) >= when_to_spawn && number_proc < total_num_of_proc) {

     // waitpid(-1, 0, WNOHANG);
      number_proc++;
      child_pid = fork();

      if (child_pid < 0) {

        printf("Could not fork!");
        return 1;
      }

      else if(child_pid > 0)  {

        when_to_spawn = SPAWN_PROC
        //printf("new proc\n");
        for (x = 0; x < total_num_of_proc; x++) {

         if(pids[x] == 0) {

            pids[x] = child_pid;
            break;
          }
        }
      }

      else  {

       //  printf("in child\n");
         //exit(0);
        execl("./slave", "slave", send_str, 0);
      }
    }

//when message received for reading a page

    if ((msgrcv(parent_queueid /*read queue*/, &sentmsg, 1024, 0, IPC_NOWAIT)) >=0) {

      x = get_pid_number(pids, total_num_of_proc, sentmsg);
      mymsg.mtype = sentmsg.mtype;
      if(x != -1) {

        total_memory_access++;

        if (num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "Received read request from process %d for page %s at time %d:%d\n", sentmsg.mtype, sentmsg.mtext, *seconds, *nano_seconds);
          num_lines++;
        }

        if (is_page_in_frame(x, sentmsg, frames)) {

          *nano_seconds = *nano_seconds + 10;

          if (num_lines < TOTAL_LINES && verbose == 1) {

            fprintf(logfile, "\t Requested page is already loaded in memory\n");
            num_lines++;
          }

          no_faults[x]++;
         /* for(i = 0; i < total_num_of_proc; i++) {

            printf("%d\n", pids[i]);
          }*/
         // printf("ugh\n");


          /*if(msgsnd(time_queueid *write queue, &mymsg, 1024,IPC_NOWAIT) != 0) {
            perror("Failed read time");
            return 1;
          }*/




          locks[x] = 1;



  // printf("ugh2\n");
//printf("here\n");


        }

        else {

//printf("Test");

          if (num_lines < TOTAL_LINES && verbose == 1) {

            fprintf(logfile, "\t Page fault! Putting request in queue\n");
            num_lines++;
          }

          page_faults[x]++;
          total_page_faults++;
          full_time = (*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds;
          enqueue(q1, sentmsg, full_time);
          in_queue[x] = in_queue[x] + 1;
        }
      }
    }

//when message received for writing a page

    if ((msgrcv(child_queueid /*write queue*/, &sentmsg, 1024, 0, IPC_NOWAIT)) >=0) {

      x = get_pid_number(pids, total_num_of_proc, sentmsg);
      if(x != -1) {

        total_memory_access++;

        r  = (x * PAGE_PER_PROCESS) + (atoi(sentmsg.mtext) / 1000);
        dirty_bit[r] = 1;

        if (num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "Received write request from process %d for page %s at time %d:%d\n", sentmsg.mtype, sentmsg.mtext, *seconds, *nano_seconds);
          num_lines++;
        }

        if (is_page_in_frame(x, sentmsg, frames)) {

          if (num_lines < TOTAL_LINES && verbose == 1) {

            fprintf(logfile, "\t Requested page is already loaded in memory\n");
            num_lines++;
          }



          no_faults[x]++;
          *nano_seconds = *nano_seconds + 10;


        /*  if(msgsnd(time_queueid write queue, &sentmsg, 1024,0) == -1) {
            perror("Failed write time");
            return 1;
          }*/


        locks[x] = 1;


        }

        else {

//printf("test\n");

          if (num_lines < TOTAL_LINES && verbose == 1) {

            fprintf(logfile, "\t Page fault! Putting request in queue\n");
            num_lines++;
          }

          page_faults[x]++;
          total_page_faults++;
          full_time = (*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds;
          enqueue(q1, sentmsg, full_time);
          in_queue[x] = in_queue[x] + 1;
        }
      }
      //printf("HI write\n");
    }

//taking page from queue

    if((!queueEmpty(q1)) && ((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds) >= (peek(q1) + 15) && memory_full(frames)) {

//     printf("stuc");

      tempmsg = dequeue(q1);

      //printf("%s, %d\n", tempmsg.mtext, tempmsg.mtype);
      x = get_pid_number(pids, total_num_of_proc, tempmsg);

      if(x != -1) {

        if (num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "Taking request from process %d for page %s from queue\n", tempmsg.mtype, tempmsg.mtext);
          num_lines++;
        }

        //printf("HI %d\n", test_number);
        test_number++;
        y = x * PAGE_PER_PROCESS;
        p_table[y + (atoi(tempmsg.mtext) / 1000)] = find_a_frame(frames);

        in_queue[x] = in_queue[x] - 1;
        //sentmsg.mtype = x;

/*printf("MADIT\n");
        if(msgsnd(time_queueid, &tempmsg, 1024,0) == -1) {
          perror("Failed1 write time");
          return 1;
        }*/



        if (num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "\t Request has been loaded into memory frame %d at time %d:%d\n", p_table[y + (atoi(tempmsg.mtext) / 1000)], *seconds, *nano_seconds);
          num_lines++;
        }


      locks[x] = 1;
      }
    }

//when process terminates

    if ((msgrcv(kill_queueid, &sentmsg, 1024, 0, IPC_NOWAIT)) >=0) {
      //printf("stuc");
      x = get_pid_number(pids, total_num_of_proc, sentmsg);

      if(x != -1) {

        if (num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "Terminating process %d", sentmsg.mtype);
          fprintf(logfile, "\t EAT: %.6f nano seconds\n", (((double)(page_faults[x]) /(page_faults[x] + (no_faults[x])) * 5) + 10));
          num_lines = num_lines + 2;
        }

        reclaim_frames(frames, x);
        in_queue[x] = 0;
        pids[x] = 0;
        locks[x] = 0;
        page_faults[x] = 0;
        no_faults[x] = 0;

        //r  = (x * PAGE_PER_PROCESS) + (atoi(m.mtext) / 1000);
        //dirty_bit[r] = 1;

        int j;
        for(j = 0; j > PAGE_PER_PROCESS; j++) {

          if(dirty_bit[j + (x * PAGE_PER_PROCESS)] == 1)
            *nano_seconds = *nano_seconds + 10;

          dirty_bit[j + (x * PAGE_PER_PROCESS)] = 0;
        }
         //this is where you would set dirty bits back

        number_proc--;
        if(msgsnd(deadlock_queueid /*write queue*/, &sentmsg, 1024,0) == -1) {
          perror("Failed write deadlock");
          return 1;
        }

      }
    }

//up the time passed if the queue is full

    if(full_queue(in_queue, total_num_of_proc)) {

      *nano_seconds = *nano_seconds + 20;
    }

//needed to pass time if no process is running
    *nano_seconds = *nano_seconds + 1;
     //printf("HI");

//seconds conversion
    if (*nano_seconds >= MULTIPLY_FOR_SECOND) {

      *nano_seconds = *nano_seconds - MULTIPLY_FOR_SECOND;
      *seconds = *seconds + 1;
    }

    //printf("HI\n");

//print the frame table
    if(*nano_seconds >= time_to_print) {


    //printf("HI\n");
      time_to_print = time_to_print + 50000;
      if(num_lines < TOTAL_LINES) {
        for (z = 0; z < TOTAL_FRAMES; z++) {

         fprintf(logfile, "%c", frames[z]);
        }
        fprintf(logfile, "\n");
        num_lines = num_lines + 2;
      }
    }

//kill after 2 seconds
    if(*seconds >= 2) {

      printf("2 seconds reached\n");
      break;
    }

//loop that checks the frame table and runs get page to free frames if needed

    for (y = 0 ; y < TOTAL_FRAMES; y++) {

      if (frames[y] == 'V' || frames[y] == 'U')
        free_frames++;
    }
    if (free_frames >= 230)
      get_page(frames);

    free_frames = 0;
    waitpid(-1, 0, WNOHANG);
  }
//kill processes

  num_proc_for_alarm = number_proc;
  kill_childs();

//remove shared memory

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_p_table, p_table) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_pids, pids) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }
  if (detachandremove(shm_id_locks, locks) == -1) {
    perror("Failed to destroy sfdsshared memory segment");
    return 1;
  }

//remove queues

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
  if(removequeue(time_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlock_queueid) < 0)
    perror("remove queue");
 /* if(removequeue(deadlockrec_queueid) < 0)
    perror("remove queue");*/

  queueDestroy(q1);

//print stats

 // statistics();

//close file


  fclose(logfile);

  return 0;
}
