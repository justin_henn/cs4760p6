//queue.h
//This is the h file for queues
//Author: Justin Henn
//Date 5/02/2017
//Assignment: 6
#ifndef QUEUE_H
#define QUEUE_H

#include <sys/types.h>

typedef struct queue{
  struct Node *head, *tail;
} Queue;

typedef struct node{
    int data;
    struct node* next;
} Node;

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;


Queue* make_queue();
mymsg_t dequeue(Queue *this_queue);
void enqueue(Queue *this_queue, mymsg_t x, int y);
void queueDestroy(Queue *q);
int queueEmpty(const struct queue *q);
int peek(Queue * q);
//mymsg_t peek_msg(Queue* q);
#endif
